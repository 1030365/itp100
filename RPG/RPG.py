import random
from random import *
import time

def wrldgen(x,y):
    handle=open('/Users/1030365/Projects/itp100/RPG/overworld.txt')
    strg=[]
    for line in handle:
        strg.append(line[:-1])
    x+=30
    y+=21
    strg=strg[len(strg)-y-9:len(strg)-y+10]
    strg2=[]
    for line in strg:
        strg2.append(line[x-14:x+15])
    strg2[9]=strg2[9][:14]+'O'+strg2[9][15:]
    return strg2

def encounter(x,y):
    strg=wrldgen(x,y)
    strg[8]=strg[8][:14]+'!'+strg[8][15:]
    return strg

def mvcheck(orient,x,y):
    strg=''
    strg2=wrldgen(x,y)
    if orient=='a':
        strg+=strg2[9][13]
    elif orient=='d':
        strg+=strg2[9][15]
    elif orient=='w':
        strg+=strg2[8][14]
    elif orient=='s':
        strg+=strg2[10][14]
    return strg


def wrldload(x,y,foe):
    print('\n'*100+'x='+str(x)+' y='+str(y))
    if foe==0:
        for line in wrldgen(x,y):
            print(line)
    elif foe==1:
        for line in encounter(x,y):
            print(line)
        print('Enter Command: ')
    elif foe==2:
        print(((('#'*29)+'\n')*19)[:-1])
        print('Enter Command: ')





def play():
    ran=1
    seed(ran)
    x=0
    y=0
    wrldload(x,y,0)
    while True:
        com=input('Enter Command: ')
        if com in 'wasd':
            if not mvcheck(com,x,y) in '#':
                if com=='w':
                    y+=1
                elif com=='a':
                    x-=1
                    ran=2*ran
                    seed(ran)
                elif com=='s':
                    y-=1
                elif com=='d':
                    x+=1
                    ran-=1
                    seed(ran)
            if randint(1,15)>1:
                wrldload(x,y,0)
            else:
                wrldload(x,y,1)
                time.sleep(0.8)
                for i in range(0,2):
                    wrldload(x,y,2)
                    time.sleep(0.1)
                    wrldload(x,y,1)
                    time.sleep(0.1)
                wrldload(x,y,2)
        if com.upper()=='QUIT':
            break


