#------RPG PROJECT-----

For this project, I am making a text based RPG. An RPG is a type of game
where you explore a world, fighting monsters in usually turn based combat,
and leveling up. These are the general tropes of RPGs. For mine, I want to create
a world using text that you can explore with dungeons to find and monsters
to beat. Here are some of the requirements I have set for myself:

1. Traversable Overworld
2. 4 dungeons with an area to explore separate from the overworld
3. A turn based combat system where the player has between 2 and 8 moves
4. 3-6 Enemy types each with 2-3 moves and are scaled according to your level
5. An experienced system that requires more experienced to level up the higher level you are
6. 4 bosses in dungeons
7. Healing items
8. Title screen

Other potential features:

1. Shop and currency
2. Zelda-like key items for overworld use
3. Save files that save after each dungeon or allow manual saves
4. Magic attacks with MP meter
5. Double monster encounters
