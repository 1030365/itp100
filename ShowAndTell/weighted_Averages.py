def wghtAvg(values):
    """
      >>> wghtAvg([50,20,80,50,90,30])
      77.0
      >>> wghtAvg([70,40,90,60])
      82.0
      >>> wghtAvg([20,91,21,1,22,8])
      20.17
    """
    even = 0
    numStore = 0
    avg = 0
    for num in values:
            if even == 0:
                    numStore = num
                    even = 1
            else:
                    avg = avg +((num / 100) * numStore)
                    even = 0
    return avg


if __name__=='__main__':
    import doctest
    doctest.testmod()
