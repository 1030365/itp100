def teach(phrase,feeling,new):
    alphabet='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    if feeling=='+':
        datafeel=1
    elif feeling=='-':
        datafeel=-1
    elif feeling=='=':
        datafeel=0
    else:
        return 'incompatable feel'
    database=dict()
    conts=''
    handle=open('/Users/1030365/Downloads/AItest.txt','r+')
    if new=='No':
        for line in handle:
            database[(line[:-1]).split()[0]]=[int((line[:-1]).split()[1]),int((line[:-1]).split()[2])]
    else:
        database=dict()
    strg=''
    for letter in phrase:
        if letter in (alphabet+' '):
            strg+=letter
    strg=strg.upper()
    strg=strg.split()
    for word in strg:
        database[word]=[database.get(word,[0])[0]+datafeel,database.get(word,[0,0])[1]+1]
    handle.truncate(0)
    for name in database.keys():
        handle.write(name+' '+str(database[name][0])+' '+str(database[name][1])+'\n')
    handle.close()


def test(phrase):
    handle=open('/Users/1030365/Downloads/AItest.txt')
    database=dict()
    for line in handle:
        strg=line[:-1].replace('\x00','').split()
        database[strg[0]]=(int(strg[1])/int(strg[2]))
    alphabet='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    strg=''
    for letter in phrase:
        if letter in (alphabet+' '):
            strg+=letter
    strg=strg.upper().split()
    phrase=strg
    for word in strg:
        database[word]=database.get(word,0)
    strg=0
    for val in database.keys():
        if val in phrase:
            strg+=database[val]
    if strg/len(phrase)>=0.3:
        return 'Positive phrase'
    elif strg/len(phrase)<=-0.3:
        return 'Negative phrase'
    else:
        return 'Neutral phrase'
