1. What function does Python use for accessing files? What arguments does it need? What does it return?

open(filename, mode) will open the file named filename and either read it (if mode is set to 'r') or write it (if mode is set to 'w')

2. What is a file handle? Explain how it relates to a file? Where is the data stored?

It is essentially wrapping paper for a file. open() can unwrap it to allow you to read what is in the file, but the handle isn't the file. It is the wrapping.

3. What is '\n'?

'\n' causes the string to be split into seperate lines. print('cool\nbeans') will print 'cool' on the first line and 'beans' on the next.

4. What does Dr. Chuck say is the most common way to treat a file when reading it?

We think of each line being a seperate string, but in reality, at the end of each line there is a '\n' to seperate the lines.

5. In the Searching Through a File (fixed) example, Dr. Chuck talks about the problem of the extra newline character that appears when we print out each line. He resolves this problem by using line.rstrip(), invoking Python's built-in rstrip method of strings. Could we also use a slice here, and write line[:-1] instead? Explain your answer.

This works because it removes the last character in the line. In this case, the last character is always'\n'

6. The second video presents three different ways, or patterns for selecting lines that start with 'From:'. Compare these three patterns, providing examples of each.

Three that I noticed were just using an if statement to see if the line starts with "From:", one used continue if it did not start with "From:", and the last one uses an in statement to see if "From:" is in the line (Ok, he doesn't actually use "From:" in that example but it accomplishes the same thing pretty much)

7. A new Python construct is introduced at the end of the second video to deal with the situation when the program attempts to open a file that isn't there. Describe this new construct and the two new keywords that pair to make it.

I actually was browsing the internet and found 2 out of 3 of these before even seeing the video. try attempts to run code but will not run it if it would result in a traceback. except runs if the try was a failiure. quit() terminates the program without an output. In the example, the program attempts to find a file named what the user inputs, but if it doesn't exist, it notifys the user and terminates.
