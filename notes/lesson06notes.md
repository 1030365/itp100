1. Dr. Chuck calls looping the 4th basic pattern of computer programming. What are the other three patterns?

sequential, conditional, and store and reuse

2. Describe the program Chr. Chuck uses to introduce the while loop. What does it do?

Dr. Chuck makes a program that types x, then decreases x by 1 until x=0 then says blastoff and then prints x (which will be equal to 0). In his example, he counts down from 5.

3. What is another word for looping introduced in the video?

iterate.

4. Dr. Chuck shows us a broken example of a loop. What is this kind of loop called? What is wrong with it?

An infinite loop. A loop that loops so many times that your mouse will turn into a looping beach ball. 

5. What Python statement (a new keyword for us, by the way) can be used to exit a loop?

The new word is break. It exits whatever loop or if statement it is in without running the code after it or looping back to the start.

6. What is the other loop control statement introduced in the first video? What does it do?

continue. instead of immeadiately exiting the loop, it loops back to the beginning of it.

7. Dr. Chuck ends the first video by saying that a while is what type of loop? Why are they called that? What is the type of loop he says will be discussed next?

A while loop is an indefinite loop because it runs until a statement is false.

8. Which new Python loop is introduced in the second video? How does it work?

the for loop is shown. when run, it repeats the code beneath it an amount of times equal to the amount of words or numbers in a list that you put in the loop, setting the variable (Dr. Chuck mostly uses 'i') to whatever is in the spot on the list the loop has reached, if that makes sense (man, wording stuff is hard...)

9. The third video introduces what Dr. Chuck calls loop idioms. What does he mean by that term? Which examples of loop idioms does he introduce in this and the fourth video?

A loop designed to do something with a list, such as find the largest number, or count how many numbers  are in the list, or look for 3's, etc. A few examples shown by Dr Chuck are as follows: looking for the largest and smallest number in a list, checking a list for 3's, finding the sum of all numbers in a list, and counting how many numbers in a list.






