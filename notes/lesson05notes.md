# Lesson 5 notes
1. How can we use Markdown and git to create nice looking documents under revision control?

We can add some programming behind the scenes that allow us to attach stuff like links and files to our words and paragraphs!

2. Dr. Chuck describes functions as store and reused pattern in programming. Explain what he means by this?

I am pretty sure he means that you can store code that you can call upon with just one line of code, and the code you save to the function stays the same unless the function uses a variable. This means you can call upon the same code you used elsewhere using just 1 line of code with the other lines of code "stored" in it.

3. How do we create functions in Python? What new keyword do we use? Provide your own example of a function written in Python. 

To make a function in python, write "def randomFunctionName(var):"
This will create a function called randomFunctionName with a variable in it call
ed 'var'. If you were to type code in the function (remember to indent it) then
a variable will be created named whatever you named var and set to whatever you 
put in place of that when using var.(example, randomFunctionName(2) will set 'var' to 2). In this scenario, var is the parameter.

4. Dr. Chuck shows that nothing is output when you define a function, what he calls the store phase. What does he call the process that makes the function run? He uses two words for this, and it is really important to understand this idea and learn the words for it.

The word used for this is either call or invoke (which one you use doesn't make a difference, they're just terms)

5. Provide some examples of built-in function in Python.

Commands in python are actually functions beleive it or not. Some examples include: 'print()', or 'type()'
