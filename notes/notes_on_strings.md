1. What does Dr. Chuck say about indexing strings? How does this operation work? Provide a few examples.

Each character in a string has an index number, with the first character being 0 and increasing from there. Example I always use for some reason:'coolio'[3]='l'

2. Which Python function returns the number or characters in a string? Provide a few examples of it being used.

len(str) is the one. It is used to find the amount of letters in a word. Example: len('coolio')=6.

3. Discuss the two ways Dr. Chuck shows to loop over the characters in a string. Provide an example of each.

You can use a while loop to run as long as your counting variable is less then the length of the string, or you can use a for loop to run for each character. Dr. Chuck creates a variable called index and the while loop says to run as long as index is greater than "len(fruit)". This loops the function setting index to whatever index number each character has. He also uses a for loop, setting letter to whatever character of banana the loop is on.

4. What is slicing? Explain it in your own words and provide several examples of slicing at work. including examples using default values for start and end.

Slicing is when you tell the program to only output a specific range of characters in a string. example: 'coolio'[1:-2]='ool' and 'coolio'[3:]='lio'

5. What is concatenation as it applies to strings. Show an example.

When you use the + operater to combine strings, it is a concatenation. Example: 'cool'+'io'='coolio'

6. Dr. Chuck tells us that in can be used as a logical operator. Explain what this means and provide a few examples of its use this way with strings.

'in' can be used to check to see if a specific sequence of characters exists in a string. Example: 'cool' in 'coolio' returns true, but 'beans' in 'coolio' returns false.

7. What happens when you use comparison operators (>, <, >=, <=, ==, !=) with strings?

Using == and != asks if the 2 strings say or do not say the same thing. Everything else uses the amount of characters in the word. If one string has more characters than another, then it is greater.

8. What is a method? Which string methods does Dr. Chuck show us? Provide several examples.

A metheod is something you can attach to a string to affect what happens to it. Dr Chuck shows us that .lower returns the string with only lowercase letters and he shows us .find which returns where in a string a sequence of characters are.

9. Define white space.

Spaces and tabs at the beginning and end of a string are known as white space. They can be removed with the .strip metheod or just the beggining or end white space can be removed with .lstrip and .rstrip respectively.

10. What is unicode?

When using strings with non latin characters, using u'str' would change your string into unicode in python2, but in python3, it stays a string. Unicode is sort of like strings, but annoyingly, behaves differently when files are involved.
