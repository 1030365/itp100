1. In the first video of this lesson, Dr. Chuck discusses two very important concepts: algorithms and data structures. How does he define these two concepts? Which one does he say we have been focusing on until now?

The two very important concepts in computer science are Algorithms and Data structures. Algorithms are the kind we have been talking about up until now, and they follow a series of steps to solve a problem. Data structures on the other hand are meant to organize data, or lay it out in a specific way.

2. Describe in your own words what a collection is.

A collection is a list that has multiple values or strings in it; It is a value that can equal more than one thing.

3. Dr. Chuck makes a very important point in the slide labeled Lists and definite loops - best pals about Python and variable names that he has made before, but which bares repeating. What is that point?

Python is picky about your choice of words. The variable "Friend" and the variable "Friends" are not the same variable.

4. How do we access individual items in a list?

You can use indexing. Each value in a list has an index number starting from 0 and counting up. You can call upon a specific value in the list by putting its index number in square brackets after the list.

5. What does mutable mean? Provide examples of mutable and immutable data types in Python.

If something is mutable, then an element of it can be changed. If it is immutable, you can't change any element of it using index, you need a whole new string.

6. Discribe several list operations presented in the lecture.

The + operator allows you to add one list onto the end of another, and you can also slice strings and take only a section of the values of a list.

7. Discribe several list methods presented in the lecture.

you can use append to add a value to a list, you can us in to check for things in a list, you can use sort to sort stuff by alphabetical order, and sum adds all values together. Those are just a few though.

8. Discribe several useful functions that take lists as arguments presented in the lecture.

Len and Sum are compatible with lists just like they are with strings.

9. The third video describes several methods that allow lists and strings to interoperate in very useful ways. Describe these.

split splits up the values in a string by using the spaces as dividers, however you can use a different character as a divider as well. Startswith allows you to check if ther very beginning of a line starts with a certainn combination of characters, and rstrip removes the \n's or newlines.

10. What is a guardian pattern? Use at least one specific example in describing this important concept.

A guardian pattern is a line or multiple lines of code preventing python from having from giving you a syntax error. One example is if you had an algorithm that divides a number by a bunch of different numbers, you could have a guardian pattern that checks if the number you are about to put as a dividend isn't 0.
