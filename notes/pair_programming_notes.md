1. Why pair program? What benefits do the videos claim for the practice?

Pair programming allows you to combine your knowledge with someone elses to come up with ways to solve a problem and allows one person to describe their algorithm and someone else translate it to python. It highlights your strengths and covers your weaknesses.

2. What are the two distinct roles used in the practice of pair programmin? How does having only one computer for two programmers aide in the pair adopting these roles?

The two roles are driver and navigator. Navigator comes up with the big idea and what the program should ultimately do and also offers suggestions to the driver on what they should add or change. The driver writes the code and solves the minor problems along the way, such as removing whitespace from a string.
