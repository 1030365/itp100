1. What are regular expressions? What are they used for? When did they originate?

Regular expressions are lines of code that search for things such as a specific sequence of characters within a bigger thing such as a string. They originated in the 1960's

2. Use Markdown to reproduce the Regular Expression Quick Guide slide.

#^        Matches the beginning of a line
#$        Matches the end of a line
#.        Matches any character
#\s       Matches any whitespace
#\S       Matches any non-whitespace character
#*        Repeats a character zero or more times
#*?       Repeats a character zero or more times (non-greedy)
#+        Repeats a character one or more times
#+?       Repeats a character one or more times (non-greedy)
#[aeiou]  Matches a single character in the listed set
#[^XYZ]   Matches a single character not in the listed set
#[a-z0-9] The set of characters can include a range
#(        Indicates where string extraction is to start
#)        Indicates where string extraction is to end

3. Make a text file with 10 lines in it. Write a python program that reads the contents of the file into a list of lines, and create a regular expression, rexp that will select exactly three items from lines.

f=open('/Users/1030365/Downloads/regex.txt')
for i in f:
    d=[]
    for a in re.findall('^h.*//',i):
        d.append(a)
    for b in re.findall('//(.*?)\.',i):
        d.append(b)
    for c in re.findall('//.*\.(.*?)/',i):
        d.append(c)
    print(d)

4. Do a web search and find a few good resources for using regular expressions in vim.

http://vimregex.com/
http://www.softpanorama.org/Editors/Vimorama/vim_regular_expressions.shtml
https://www.ele.uri.edu/faculty/vetter/Other-stuff/vi/vimtips.html
