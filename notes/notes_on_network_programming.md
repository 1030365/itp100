1. Dr. Chuck mentions the architecture that runs our network. What is the name of this architecture?

This network is called the transport control protocal (TCP) architechture

2. Take a close look at the slide labelled Stack Connections. Which layer does Dr. Chuck say we will be looking at, assuming that one end of this layer can make a phone call to the corresponding layer on the other end? What are the names of the two lower level layers that we will be abstracting away?

The lower level layers that Dr. Chuck isn't focusing on yet are the link layer and internet layer. The one we are going to learn about it the transport layer.

3. We will be assuming that there is a ____________ that goes from point A to point B. There is a ______________ running at each end of the connection. Fill in the blanks.

Pipe, Socket

4. Define Internet socket as discussed in the video.

Dr. Chuck says the "data phone call" is called a socket. The socket is the connection between two computers that allow them to share data with each other.

5. Define TCP port as discussed in the video.

TCP ports are extensions of a web server that contain the various sevices of the server. They also each have their own port number.

6. At which network layer do sockets exist?

Sockets exist on the application layer.

7. Which network protocol is used by the World Wide Web? At which network layer does it operate?

The world wide web uses the application layer, and the Hypertext transport protocal is the one used by the world wide web most of the time. 
