1. Dr. Chuck begins this lesson by stating that tuples are really a lot like lists. In what ways are tuples like lists? Do you agree with his statement that they are a lot alike?

Tuples are almost exactly the same as lists other than the fact that you cannot edit or modify them after you make them. Its hard to not agree that they are very very similar. 

2. How do tuples essentially differ from lists? Why do you think there is a need for this additional data type if it is similar to a list?

They are like lists but are unable to be modified. This is important when you are working with a program that takes a while to go through each of your lists, you can use tuples because they allow the computer to run faster.

3. Dr. Chuck says that tuple assignment is really cool (Your instructor completely agrees with him, btw). Describe what tuple assignment is, showing a few examples of it in use. Do you think it is really cool? Why or why not?

Tuple assignments are great! They allow you to make a tuple of variables and then set them to a tuple of values, and each variable will be set to the corresponding value in the other tuple. I create many empty variables for later use in my programs, so this will allow me to set all of them at once!

4. Summarize what you learned from the second video in this lesson. Provide example code to make support what you describe.

I learned a few things, but the main thing was the 'sorted' function. It allows you to sort values in a list or dictionary in ascending or descending order. Dr. Chuck uses this when he has a list of tuples containing the number first, then the key. He uses it to sort these tuples from highest number in the tuple to lowest number. He then prints it in a flipped version so the keys come first and then the number.

5. In the slide titled Even Shorter Version Dr. Chuck introduces list comprehensions. This is another really cool feature of Python. Did the example make sense to you? Do you think you understand what is going on?

The example made sense after he explained the for loop. What is happening is python is running a for loop within a list in order to decide the tuples in that list. I think it is cool that we have the option to do that.
