1. Describe the characteristics of a collection.

They are a bunch of values in one variable that can be one of many of the values in the variable. Some good things to use with it are 'in' and 'for' because they check for a specific value in the collection or run once per value with a variable set to each value in the collection.

2. Using the video for inspiration, make a high level comparision of lists and dictionaries. How should you think about each of them?

Lists are a list of values in order. To call a specific value, you have to ask the program for a specific index number in the list to find it. With dictionaries, you can think of them as a purse filled with stuff that all have names on them. Those names are keys, and you can call for the keys to get the content the key is labeling.

3. What is the synonym Dr. Chuck tells us for dictionaries, that he presents in the slide showing a similiar feature in other programming languages?

They are similar to arrays, which show up in other programing languages. By the way, I accidently added multiple v.1 versions of this markdown file, so if you see that, it was an accident.

4. Show a few examples of dictionary literals being assigned to variables.

Dr. Chuck uses 'ooo' as an empty dictionary and jjj to contain values for 'chuck','fred', and 'jan'. Here is an example from me: sec={ 'second' : 1 , 'minute' : 60 , 'hour' : 3600}

5. Describe the application in the second video which Dr. Chuck says is one of the most common uses of dictionaries.

Dr. Chuck talks about Histograms and how they can be used to count the amount of each word there are in a list of words. He describes the process you would go through to do this in the video and mentions how your though process differs when you see the words one at a time rather than all at once.

6. Write down the code Dr. Chuck presents run this application.

counts=dict()
names=['csev','cwen','csev','zqian','cwen']
for name in names:
    if name not in counts:
        counts[name]=1
    else:
        counts[name]=counts[name]+1
print(counts)

What this does is go throught the list 'names' and counts how many of each word there is. Everytime there is a new word, a new variable is created and set to 1. When a previous word returns, the counter for that variable increases by 1.

7. What is the dictionary method that makes this common operation shorter (from 4 lines to 1)? Describe how to use it.

Using the 'get' metheod makes life much easier. It takes two arguments, being the key you are looking for, and a default value. If the key name exists, you get back the key's value. If not, a new key is created containing the default value. if you set the default value to 0, then when making a histogram, listName.get(name, 0)+1 increases an existing key by 1 if it exists and creates a new variable set to 1 if it doesn't exist.

8. Describe the application that Dr. Chuck leads into in the third video and codes in front of us in the fourth video.

He leads into dejaVu.py (All jokes aside, it is great that we finally know how that function works) The clown.txt reader function thing in a nutshell looks for what is the most common word by making a list of the words in the file, then creates a dictionary containing the words in the txt file and how many of them there are, then goes through each one looking for the word with the highest value, then prints the winner.
