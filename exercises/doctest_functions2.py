def remove_sub(sub, s):
    """
      >>> remove_sub('an', 'banana')
      'bana'
      >>> remove_sub('cyc', 'bicycle')
      'bile'
      >>> remove_sub('iss', 'Mississippi')
      'Missippi'
      >>> remove_sub('egg', 'bicycle')
      'bicycle'
    """
    start = s.find(sub)
    if start != -1:
        return s[:start] + s[start + len(sub):]
    return s


if __name__ == '__main__':
    import doctest
    doctest.testmod()
