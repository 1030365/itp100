def cipher(test):
    handle=open(test)
    alphabet='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    conts=''
    linenum=0
    for line in handle:
        if line=='STOP\n':
            break
        elif not linenum==0:
            conts+=line
        else:
            key=int(line[:-1])
            linenum+=1
    output=''
    for char in conts:
        if not char in alphabet:
            output+=char
        else:
            count=0
            while not char==alphabet[count]:
                count+=1
            if count>25:
                caps=True
            else:
                caps=False
            count+=key
            if count>25 and not caps:
                count-=26
            elif count>51:
                count-=26
            output+=alphabet[count]
    print(output[:-1])
