import xml.etree.ElementTree as ET

data='''
<media>
    <stream service="Disney+">
        <movie name="Soul">
            <rating>8/10</rating>
        </movie>
        <movie name="Avengers">
            <rating>7/10</rating>
        </movie>
        <show name="Mandolorian">
            <rating>10/10</rating>
        </show>
    </stream>
    <stream service="Netflix">
        <show name="Stranger Things">
            <rating>10/10</rating>
        </show>
    </stream>
    <games>
        <game name="Breath of the Wild">
            <rating>10/10</rating>
        </game>
        <game name="Mario 64">
            <rating>7/10</rating>
        </game>
    </games>
</media>'''
data2=ET.fromstring(data)
services=data2.findall('stream')
movies=0
shows=0
for item in services:
    for movie in item.findall('movie'):
        print(item.get('service')+' has a movie called '+movie.get('name')+'. Liam gave it a '+movie.find('rating').text)
        movies+=1
    for show in item.findall('show'):
        print(item.get('service')+' has a show called '+show.get('name')+'. Liam gave it a '+show.find('rating').text)
        shows+=1
games=data2.findall('games/game')
for game in games:
    print(game.get('name')+' is a game Liam gave a '+game.find('rating').text+'.')
print('Liam reveiwed '+str(len(games))+' games.')
print('Liam reveiwed '+str(movies)+' movies.')
print('Liam reveiwed '+str(shows)+' shows.')
