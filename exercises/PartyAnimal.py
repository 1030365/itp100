class PartyAnimal:
    x=0
    def __init__(self,name):
        self.name=name

    def party(self):
        self.x+=1
        print(f'So far, {self.x} animal(s) are at the party named {self.name}.')

an=PartyAnimal(input('All animals at the party will be named: '))
for count in range(0,3):
    an.party()
