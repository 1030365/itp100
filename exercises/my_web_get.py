#!/usr/bin/env python3
import os
import sys
from urllib import request, parse

try:
    raw_url = sys.argv[1]
    webfile = request.urlopen(raw_url)
    url = parse.urlparse(raw_url)
    filename = os.path.basename(url.path)
    outfile = open(filename, 'w')
    for line in webfile:
        outfile.write(line.decode())
    outfile.close()
except IndexError:
    print("Must include url as argument.")
except TypeError:
    print("url must be in format: scheme://host/path")
except:
    print(f"ERROR: {raw_url} not found.")

