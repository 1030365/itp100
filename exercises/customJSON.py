import json

data='''
{
    "title" : "Ranking the Smash Bros Anime Swordfighters",
    "disclaimer" : "I am talking stricktly about how these characters are in smash ultimate and this is just my opinion.",
    "characters" : {
        "game" : {
            "Melee" : {
                "Marth" : {
                    "rating" : "7/10",
                    "opinion" : "Very basic, but he is probably the most well known Fire Emblem character, and his tippers and midair controls feel nice."
                },
                "Roy" : {
                    "rating" : "10/10",
                    "opinion" : "He may be pretty basic, but landing the sweetspot on his attacks is increadibly satisfying."
                }
            },
            "Brawl" : {
                "Ike" : {
                    "rating" : "7/10",
                    "opinion" : "He is not the most fun to control because of how heavy he feels, but his attacks are very satisfying to land and I may be blinded by nostalgia and I know I said I would only talk about Ultimate, but his subspace emmissary appearence was so cool the first time I saw it!"
                }
            },
            "4" : {
                "Lucina" : {
                    "rating" : "3/10",
                    "opinion" : "Imagine Marth but with none of the stuff that makes him interesting. Honestly, I think Lucina is the most generic character in the game. She feels fine to play, but she is so basic that it is a bit boring."
                },
                "Robin" : {
                    "rating" : "5/10",
                    "opinion" : "Robin is like the opposite of Lucina. He is super unique with his spells and levin sword, but is possibly one of the most complicated characters to play in the entire game. He probably fun to play if you know what you are doing with him, but I do not."
                },
                "Shulk" : {
                    "rating" : "8/10",
                    "opinion" : "He felt too sluggish in smash 4, but in Ultimate, he feels great! He is a bit complicated to get good at, but it is pretty fun to have a laser sword that lets you become almost any character archetype"
                },
                "Cloud" : {
                    "rating" : "9/10",
                    "opinion" : "He is fun to play as, and his limit mechanic is really cool, but his hits feel a bit less satisfying to land as I feel like they should."
                },
                "Corrin" : {
                    "rating" : "5/10",
                    "opinion" : "He exists."
                },
            },
            "Ultimate" : {
                "Chrom" : {
                    "rating" : "7/10",
                    "opinion" : "He is fun, and I prefer his up B compared to Roy, but I find Roy more fun overall."
                },
                "Hero" : {
                    "rating" : "10/10",
                    "opinion" : "Down B. Just Down B."
                },
                "Byleth" : {
                    "rating" : "8/10",
                    "opinion" : "Very traditional feeling, but the moveset is pretty fun to play with. Also, the male green haired Byleth is my favorite alternate costume in Ultimate."
                },
                "Sephiroth" : {
                    "rating" : "9/10",
                    "opinion" : "Like Cloud, he is super fun to play, but something feels off. Unlike Cloud, I actually am not sure what it is with Sephiroth. His attacks feel satisfying to land, so I am not sure."
                },
                "Pyra" : {
                    "rating" : "?/10",
                    "opinion" : "She is not out yet, but her smash attacks look like some of the most satisfying attacks to land in all of smash."
                },
                "Mythra" : {
                    "rating" : "?/10",
                    "opinion" : "Also not out yet, but the special moves look really great."
                }
            }
        }
    }
}'''

info=json.loads(data)
print(info["characters"]["game"]["Ultimate"]["Mythra"]["opinion"])
