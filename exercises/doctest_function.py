def add_first_two(a, b, c):
    """
      >>> add_first_two(1, 1, 1)
      2
      >>> add_first_two(3, 8, 5)
      11
      >>> add_first_two(-2, -2, 4)
      -4
    """
    return a + b


if __name__=='__main__':
    import doctest
    doctest.testmod()


#Hi Mr. Elkner! My teammate wrote this code basically identically but it didn't work for him when he did the doctest and I don't know why. It works for me though.
