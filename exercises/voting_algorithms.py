def plurality(election):
    """
    Return the plurality winner among three candidates, 1, 2, and 3. The input
    election is a list of three tuples, so in a plurality election only the
    first element matters.

      >>> plurality([(1, 2, 3), (2, 3, 1), (2, 1, 3)])
      2
      >>> plurality([(3, 2, 1), (3, 2, 1), (2, 1, 3)])
      3
    """
    # Count the votes for each of the three candidates in a dictionary
    vote_totals = {1: 0, 2: 0, 3: 0} 
    for vote in election:
        vote_totals[vote[0]] += 1

    # Return the candidate number with the highest vote count
    return max(vote_totals.items(), key=lambda x: x[1])[0]

def exhaustive(election):
    """
      >>> exhaustive([(1, 2, 3), (1, 2, 3), (1, 2, 3), (2, 1, 3), (2, 3, 1), (3, 1, 2)])
      1
      >>> exhaustive([(1, 3, 2), (1, 3, 2), (1, 3, 2), (1, 2, 3), (1, 3, 2), (3, 2, 1), (2, 3, 1), (2, 3, 1), (2, 3, 1), (3, 2, 1), (3, 2, 1), (2, 3, 1)])
      2
    """
    primary=dict()
    winner=1
    loser=1
    for vote in election:
        primary[vote[0]]=primary.get(vote[0],0)+1
    for votes in primary.keys():
        if primary[votes]<primary[loser]:
            loser=votes
    seconds=dict()
    for vote in election:
        if vote[0]==loser:
            primary[vote[1]]=primary.get(vote[1],0)+1
    for votes in primary.keys():
        if primary[votes]>primary[winner]:
            winner=votes
    return winner


def canVsCanPrime(election,can1,can2):
    """
    >>> canVsCanPrime([(1, 2, 3), (1, 2, 3), (1, 3, 2)], 1, 2)
    1
    >>> canVsCanPrime([(1, 2, 3), (1, 2, 3), (1, 3, 2)], 1, 3)
    1
    >>> canVsCanPrime([(1, 2, 3), (1, 2, 3), (1, 3, 2)], 2, 3)
    1
    """
    primary=dict()
    for num in range(1,4):
        if not can1==num and not can2==num:
            can3=num
        primary[can1]=0
        primary[can2]=0
    for vote in election:
        if not vote[0]==can3:
            primary[vote[0]]=primary.get(vote[0],0)+1
        else:
            primary[vote[1]]=primary.get(vote[1],0)+1
    if primary[can1]>primary[can2]:
        loser=can2
        winner=can1
    else:
        loser=can1
        winner=can2
    primary=dict()
    primary[winner]=0
    primary[can3]=0
    for vote in election:
        if loser==vote[0]:
            primary[vote[1]]=primary.get(vote[1],0)+1
        else:
            primary[vote[0]]=primary.get(vote[0],0)+1
    for total in primary.keys():
        if primary[total]>primary[winner]:
            winner=total
    return winner


if __name__ == "__main__":
    import doctest
    doctest.testmod()
